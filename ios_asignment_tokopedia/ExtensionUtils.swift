//
//  ExtensionUtils.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    convenience init(string: String) {
        var chars = Array(string.hasPrefix("#") ? string.characters.dropFirst() : string.characters)
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 1
        switch chars.count {
        case 3:
            chars = [chars[0], chars[0], chars[1], chars[1], chars[2], chars[2]]
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            alpha = 0
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}

extension UIView {
    
    func setZeroData(isHidden: Bool) {
        let label = UILabel()
        label.text = "Data tidak ditemukan"
        label.textColor = .black
        label.frame = CGRect(x: self.center.x, y: self.center.y, width: self.frame.size.width, height: 20)
        label.textAlignment = .center
        label.center = self.center
        label.isHidden = isHidden
        self.addSubview(label)
    }
}
