//
//  Endpoint.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation


class Endpoint {
    
    let URL = "https://ace.tokopedia.com/search/v2.5/"
    let productUrl = "product?"
    
    
    func productAPI(pMin: String, pMax: String, wholeSale: Bool, official: Bool, fShop: String, index: Int) -> String {
        let url = "\(URL)\(productUrl)q=samsung&pmin=\(pMin)&pmax=\(pMax)&wholesale=\(wholeSale)&official=\(official)&start=\(index)&rows=10&fshop=\(fShop)"
        
        print("url \(url)")
        
        return url
    }
}

class Query {
    static let query = "q="
    static let pmin = "pmin="
    static let pmax = "pmax="
    static let wholesale = "wholesale="
    static let official = "official="
    static let start = "start="
    static let fshop = "fshop="
}
