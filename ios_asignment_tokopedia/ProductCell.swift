//
//  ProductCell.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 06/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    func setupData() {
        
    }
}
