//
//  Connection.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import Alamofire

protocol MainCallback{
    func onSuccessResponse<T>(message: String, response: DataResponse<T>)
    //func onFailedResponse<T>(message: String, object: AnyObject, response: DataResponse<T>)
    //func onFailureResponse(message: String, object: AnyObject)
}

class ConnectionManager {
    
    var mainCallback: MainCallback?
    var view: UIView!
    
    func connectionCallback<T>(responseData: DataResponse<T>) -> Void{
        let statusResponse = responseData.result
        var message = ""
        
        print("status code \(responseData.response?.statusCode)")
        
        if statusResponse.isSuccess {
            let mainResponse = responseData.result.value as? StatusResponse
            message = mainResponse?.status?.message ?? ""
            
            print("message \(message)")
            if let httpStatusCode = responseData.response?.statusCode {
                if httpStatusCode == 200 {
                    mainCallback?.onSuccessResponse(message: message, response: responseData)
                } else {
                    print("failed")
                   // view.showToast(message: message)
                }
            }
        } else {
            print("failure")
//            if progressDialog != nil {
//                progressDialog.dismissProgressDialog()
//            }
//            view.showToast(message: "\(responseData.response?.statusCode ?? 0)")
//            print("connection failed ")
        }
        
    }
}
