//
//  FilterProtocol.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

protocol FilterViewProtocol: class {
    func dismissFilterPage()
    func resetFilterPage()
    
    func showCurrentData()
    func wholeSaleSwitchValueChange(_ sender: UISwitch)
}

protocol FilterPresenterProtocol: class {
    var view: FilterViewProtocol? { get set }
    var router: FilterRouterProtocol? { get set }
    
    func sendDataBackToProductPage(delegate: FilterResultProtocol, data: [String:Any])
    func presentToShopTypePage(delegate: ShopTypeProtocol?, data: [String:Any]?)
}

protocol FilterRouterProtocol: class {
    static func initPresenter(vc: FilterViewController) -> FilterPresenterProtocol
    func presentToShopTypePage(view: FilterViewProtocol, delegate: ShopTypeProtocol?, data: [String:Any]?)
    
}
