//
//  FilterPresenter.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

class FilterPresenter: FilterPresenterProtocol {
    var view: FilterViewProtocol?
    
    var router: FilterRouterProtocol?
    
    func sendDataBackToProductPage(delegate: FilterResultProtocol, data: [String : Any]) {
        
    }
    
    func presentToShopTypePage(delegate: ShopTypeProtocol?, data: [String:Any]?) {
       router?.presentToShopTypePage(view: view!, delegate: delegate, data: data)
    }
    
    
    
}
