//
//  FilterViewController.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 06/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit
import MultiSlider
import TagListView

class FilterViewController: UIViewController {
    
    //@IBOutlet weak var horizontalMultiSlider: MultiSlider!
    @IBOutlet weak var minimumPriceLbl: UILabel!
    @IBOutlet weak var maximumPriceLbl: UILabel!
    @IBOutlet weak var wholeSaleSwitch: UISwitch!
    @IBOutlet weak var tagListView: TagListView!
    
    @IBOutlet weak var multiSlider: MultiSlider!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    
    var delegate: FilterResultProtocol? = nil
    var data = [String: Any]()
    var presenter: FilterPresenterProtocol? = nil
    
    var minPrice: Double?
    var maxPrice: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = FilterRouter.initPresenter(vc: self)
        
        tagListView.delegate = self
        
        multiSlider.orientation = .horizontal
        multiSlider.minimumValue = 100000
        multiSlider.maximumValue = 6000000
        multiSlider.thumbCount = 2
        multiSlider.snapStepSize = 200000
        multiSlider.trackWidth = 5
        
        multiSlider.tintColor = UIColor(string: "#24CB2A")
        
        closeBtn.addTarget(self, action: #selector(dismissFilterPage), for: .touchUpInside)
        multiSlider.addTarget(self, action: #selector(multiSliderValueChange(_:)), for: .valueChanged)
        wholeSaleSwitch.addTarget(self, action: #selector(wholeSaleSwitchValueChange(_:)), for: .valueChanged)
        showCurrentData()
    }
    
    @IBAction func resetData(_ sender: Any) {
        resetFilterPage()
    }
    
    @objc func multiSliderValueChange(_ slider: MultiSlider) {
        minPrice = Double(slider.value[0])
        maxPrice = Double(slider.value[1])
        
        let minPriceFormatted = Utils.shared.getCurrencyLocaleFormat(amount: minPrice ?? 0)
        let maxPriceFormatted = Utils.shared.getCurrencyLocaleFormat(amount: maxPrice ?? 0)
        
        minimumPriceLbl.text = "\(minPriceFormatted)"
        maximumPriceLbl.text = "\(maxPriceFormatted)"
        
        data[Query.pmin] = "\(minPrice ?? 0)"
        data[Query.pmax] = "\(maxPrice ?? 0)"
    }

    @IBAction func shopTypeBtn(_ sender: Any) {
        presenter?.presentToShopTypePage(delegate: self, data: self.data)
    }
    
    @IBAction func applyFilter(_ sender: Any) {

        delegate?.setResultForFilter(data: data)
        dismissFilterPage()
    }
}

extension FilterViewController: FilterViewProtocol {
    @objc func dismissFilterPage() {
        dismiss(animated: true, completion: nil)
    }
    
    //Function to reset ALL Data
    func resetFilterPage() {
        data = Utils.shared.setIntialData()
        showCurrentData()
    }
    
    //Show Data from Search Controller
    func showCurrentData() {
        let pmin = data[Query.pmin] as? String
        let pMax = data[Query.pmax] as? String
        let official = data[Query.official] as? Bool
        let fshop = data[Query.fshop] as? String
        let wholesale = data[Query.wholesale] as? Bool
        
        wholeSaleSwitch.isOn = wholesale ?? false
        
        if fshop == "2" {
            tagListView.addTag("Gold Merchant")
        } else {
            tagListView.removeTag("Gold Merchant")
        }
        
        if official! {
            tagListView.addTag("Official Store")
        } else {
            tagListView.removeTag("Official Store")
        }
        
        guard let min = NumberFormatter().number(from: pmin ?? "") else { return }
        guard let max = NumberFormatter().number(from: pMax ?? "") else { return }

        multiSlider.value = [min, max] as! [CGFloat]
        
        let minPriceFormatted = Utils.shared.getCurrencyLocaleFormat(amount: Double(truncating: min))
        let maxPriceFormatted = Utils.shared.getCurrencyLocaleFormat(amount: Double(truncating: max))
        
        minimumPriceLbl.text = "\(minPriceFormatted)"
        maximumPriceLbl.text = "\(maxPriceFormatted)"
    }
    
    //Set wholesale
    @objc func wholeSaleSwitchValueChange(_ sender: UISwitch) {
        let isOn = sender.isOn
        data[Query.wholesale] = isOn
    }
}

extension FilterViewController: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        if tagView.titleLabel?.text == "Gold Merchant" {
            data[Query.fshop] = "1"
        } else {
            data[Query.official] = false
        }
        
        tagListView.removeTagView(tagView)
    }
}

extension FilterViewController: ShopTypeProtocol {
    func isGoldMerchantChecked(isCheck: Bool) {
        let tags = tagListView.tagViews
        if isCheck {
            data[Query.fshop] = "2"
            
            if tags.first(where: {$0.titleLabel?.text == "Gold Merchant"}) == nil{
                tagListView.addTag("Gold Merchant")
            }
            
        } else {
            data[Query.fshop] = "1"
            if let tag = tags.first(where: {$0.titleLabel?.text == "Gold Merchant"}) {
                tagListView.removeTagView(tag)
            }
        }
    }
    
    func isOfficialStoreChecked(isCheck: Bool) {
        let tags = tagListView.tagViews
        data[Query.official] = isCheck
        
        if isCheck {
            if tags.first(where: {$0.titleLabel?.text == "Official Store"}) == nil{
                tagListView.addTag("Official Store")
            }
        } else {
            if let tag = tags.first(where: {$0.titleLabel?.text == "Official Store"}) {
                tagListView.removeTagView(tag)
            }
        }
    }
}

