//
//  FilterRouter.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

class FilterRouter: FilterRouterProtocol {
    static func initPresenter(vc: FilterViewController) -> FilterPresenterProtocol {
        let presenter: FilterPresenterProtocol = FilterPresenter()
        let router: FilterRouterProtocol = FilterRouter()
        
        presenter.view = vc
        presenter.router = router
        
        return presenter
    }
    
    func presentToShopTypePage(view: FilterViewProtocol, delegate: ShopTypeProtocol?, data: [String:Any]?) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShopTypeViewController") as! ShopTypeViewController
        vc.shopTypeDelegate = delegate
        vc.data = data!
        
        if let source = view as? UIViewController {
            source.present(vc, animated: true, completion: nil)
        }
    }
}
