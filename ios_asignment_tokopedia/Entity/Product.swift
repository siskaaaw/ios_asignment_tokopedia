//
//  Product.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductData: StatusResponse {
    var data: [Product]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        
    }
}


class Product: Mappable {
    
    var name: String?
    var imageUri: String?
    var price: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        imageUri <- map["image_uri"]
        price <- map["price"]
    }
}
