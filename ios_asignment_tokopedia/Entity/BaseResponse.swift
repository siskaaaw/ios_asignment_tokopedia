//
//  BaseResponse.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import ObjectMapper

class StatusResponse: Mappable {
    var status: BaseResponse?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
    }
}

class BaseResponse: Mappable {
    
    var errorCode: Int?
    var message: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        errorCode <- map["error_code"]
        message <- map["message"]
    }
    
    
}
