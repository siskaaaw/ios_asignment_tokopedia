//
//  SearchProtocol.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 06/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

protocol SearchViewProtocol: class {
    func showActivityIndicator()
    func dismissActivityIndicator()
    func presentToFilterPage()
    
    func callProductAPI()
    func setProducts(products: [Product]?)
    
}

protocol SearchPresenterProtocol: class {
    var view: SearchViewProtocol? { get set }
    var interactor: SearchInteractorInputProtocol? { get set }
    var router: SearchRouterProtocol? { get set }
    
    func presentToFilterPage(delegate: FilterResultProtocol, data: [String:Any])
    
    func callProductAPI(data: [String: Any])
}

protocol SearchRouterProtocol: class {
    static func initPresenter(vc: SearchViewController) -> SearchPresenterProtocol
    
    func presentToFilterPage(view: SearchViewProtocol, delegate: FilterResultProtocol, data: [String:Any])
}
