//
//  SearchPresenter.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation

class SearchPresenter: SearchPresenterProtocol {
    var interactor: SearchInteractorInputProtocol?
    
    var view: SearchViewProtocol?
    var router: SearchRouterProtocol?
    
    func presentToFilterPage(delegate: FilterResultProtocol, data: [String:Any]) {
        router?.presentToFilterPage(view: view!, delegate: delegate, data: data )
    }
    
    func callProductAPI(data: [String : Any]) {
        interactor?.callProductAPI(data: data)
    }
}

extension SearchPresenter: SearchInteractorOutputProtocol {
    func onSuccessGetProduct(products: [Product]?) {
        view?.setProducts(products: products)
    }
    
    func onFailedGetProduct() {
        
    }
    
    
}

