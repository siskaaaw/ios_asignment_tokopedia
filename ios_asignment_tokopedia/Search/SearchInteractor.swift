//
//  SearchInteractor.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol SearchInteractorInputProtocol: class {
    var presenter: SearchInteractorOutputProtocol? { get set }
    
    func callProductAPI(data: [String:Any])
}

protocol SearchInteractorOutputProtocol: class {
    func onSuccessGetProduct(products: [Product]?)
    func onFailedGetProduct()
}

class SearchInteractor: SearchInteractorInputProtocol, MainCallback {
    
    var presenter: SearchInteractorOutputProtocol?
    var endpoint = Endpoint()
    let connection = ConnectionManager()
    
    func callProductAPI(data: [String : Any]) {
        let pMin = data[Query.pmin] as! String
        let pMax = data[Query.pmax] as! String
        let wholesale = data[Query.wholesale] as! Bool
        let official = data[Query.official] as! Bool
        let fshop =  data[Query.fshop] as! String
        let start = data[Query.start] as! Int
        let productUrl = endpoint.productAPI(pMin: pMin,
                                             pMax: pMax,
                                             wholeSale: wholesale,
                                             official: official,
                                             fShop: fshop,
                                             index: start)
        
        Alamofire.request("\(productUrl)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<ProductData>) in
            self.connection.mainCallback = self
            self.connection.connectionCallback(responseData: response)
        }
    }
    
    func onSuccessResponse<T>(message: String, response: DataResponse<T>) {
        let result = response.result.value as! ProductData
        let products = result.data
        
        presenter?.onSuccessGetProduct(products: products)
    }
}
