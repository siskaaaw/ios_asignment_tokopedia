//
//  ViewController.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 06/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import UIKit
import Kingfisher

protocol FilterResultProtocol {
    func setResultForFilter(data: [String:Any])
}

class SearchViewController: UIViewController {

    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var filterBtn: UIButton!
    
    var presenter: SearchPresenterProtocol? = nil
    var dataQuery = [String : Any]()
    var products = [Product]()
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        presenter = SearchRouter.initPresenter(vc: self)
        productCollectionView.backgroundColor = UIColor(string: "#E8E8E8")
        
        dataQuery = Utils.shared.setIntialData()
        
        filterBtn.addTarget(self, action: #selector(presentToFilterPage), for: .touchUpInside)
        
        callProductAPI()
    }
}

extension SearchViewController: SearchViewProtocol {
    func callProductAPI() {
        presenter?.callProductAPI(data: dataQuery)
    }
    
    func showActivityIndicator() {
        
    }
    
    func dismissActivityIndicator() {
        
    
    }
    
    @objc func presentToFilterPage() {
        presenter?.presentToFilterPage(delegate: self, data: dataQuery)
    }
    
    func setProducts(products: [Product]?) {
        
        for product in products! {
            self.products.append(product)
        }

        if self.products.count == 0 {
            view.setZeroData(isHidden: false)
        } else {
            view.setZeroData(isHidden: true)
        }

        DispatchQueue.main.async {
            self.productCollectionView.delegate = self
            self.productCollectionView.dataSource = self
            self.productCollectionView.reloadData()
        }
       
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath) as! ProductCell
        
        if indexPath.row == (products.count) - 1 {
            dataQuery[Query.start] = (dataQuery[Query.start] as! Int) + 10
            callProductAPI()

            return cell
        }
        
        let product = products[indexPath.row]
        
        if let imageUrl = product.imageUri {
            cell.productImageView.kf.indicatorType = .activity
            cell.productImageView.kf.setImage(with: URL(string: imageUrl))
        }
        
        cell.productPriceLbl.text = product.price
        cell.productTitleLbl.text = product.name
        
        return cell
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 2) - 4, height: (collectionView.frame.size.width / 2) + 60)
    }
}

extension SearchViewController: FilterResultProtocol {
    func setResultForFilter(data: [String : Any]) {
        products.removeAll()
        productCollectionView.reloadData()
        
        self.dataQuery = data
        dataQuery[Query.start] = 0
        callProductAPI()
    }
}




