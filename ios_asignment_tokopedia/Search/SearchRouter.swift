//
//  SearchRouter.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 08/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

class SearchRouter: SearchRouterProtocol {
    static func initPresenter(vc: SearchViewController) -> SearchPresenterProtocol {
        let presenter: SearchPresenterProtocol & SearchInteractorOutputProtocol = SearchPresenter()
        let interactor: SearchInteractorInputProtocol = SearchInteractor()
        let router: SearchRouterProtocol = SearchRouter()
        
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return presenter
    }
    
    func presentToFilterPage(view: SearchViewProtocol, delegate: FilterResultProtocol, data: [String:Any]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        vc.delegate = delegate
        vc.data = data
        
        if let source = view as? UIViewController {
            source.present(vc, animated: true, completion: nil)
        }
    }
}
