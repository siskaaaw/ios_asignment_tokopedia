//
//  Utils.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation

class Utils {
    static let shared = Utils()
    
    func getCurrencyLocaleFormat(amount: Double) -> String {
        let formatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        formatter.numberStyle = .currency
        
        
        if let formattedTipAmount = formatter.string(from: amount as NSNumber) {
            let addSpaceSymbol = formattedTipAmount.replacingOccurrences(of: "Rp", with: "Rp ")
            return addSpaceSymbol
        }else{
            return ""
        }
    }
    
    func setIntialData() -> [String:Any]{
        let data: [String: Any] = [Query.pmin : "100000",
                                   Query.pmax : "6000000",
                                   Query.official : false,
                                   Query.fshop : "1",
                                   Query.wholesale : false,
                                   Query.start : 0]
        
        return data
    }
}
