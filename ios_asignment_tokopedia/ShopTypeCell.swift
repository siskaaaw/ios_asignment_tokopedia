//
//  ShopTypeCell.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

class ShopTypeCell: UITableViewCell {
    
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var shopTyoeLbl: UILabel!
    
    func initiateCell(shopType: ShopType?) {
        if shopType != nil {
            if (shopType?.isCheck)! {
                checkBoxBtn.setImage(UIImage(named: "ic_checkbox_check"), for: .normal)
            } else {
                checkBoxBtn.setImage(UIImage(named: "ic_checkbox_blank"), for: .normal)
            }
            
            shopTyoeLbl.text = shopType?.name 
        }
        
    }
    
}
