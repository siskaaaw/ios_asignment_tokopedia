//
//  ShopTypeProtocol.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation

protocol ShopTypeViewProtocol: class {
    func setShopTypeData()
    func setShopTypeCurrentData()
}
