//
//  ShopTypeViewController.swift
//  ios_asignment_tokopedia
//
//  Created by Yesiska Putri on 09/10/18.
//  Copyright © 2018 yesiska. All rights reserved.
//

import Foundation
import UIKit

protocol ShopTypeProtocol {
    func isGoldMerchantChecked(isCheck: Bool)
    func isOfficialStoreChecked(isCheck: Bool)
}

class ShopTypeViewController: UIViewController {
    
    var shopTypes = [ShopType]()
    
    var shopTypeDelegate: ShopTypeProtocol?
    var data = [String:Any]()
    
    @IBOutlet weak var shopTypeTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setShopTypeData()
        setShopTypeCurrentData()
        
        shopTypeTableView.delegate = self
        shopTypeTableView.dataSource = self
        shopTypeTableView.allowsMultipleSelection = true
    }
    
    @IBAction func resetPage(_ sender: Any) {
        shopTypes.removeAll()
        setShopTypeData()
        shopTypeTableView.reloadData()
    }
    
    @IBAction func closePage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func applyShopType(_ sender: Any) {
        shopTypeDelegate?.isGoldMerchantChecked(isCheck: shopTypes[0].isCheck ?? false)
        shopTypeDelegate?.isOfficialStoreChecked(isCheck: shopTypes[1].isCheck ?? false)
        dismiss(animated: true, completion: nil)
    }
}

extension ShopTypeViewController: ShopTypeViewProtocol {
    func setShopTypeData() {
        let shopTypeGold = ShopType()
        shopTypeGold.name = "Gold Merchant"
        shopTypeGold.isCheck = false
        
        let shopTypeOfficial = ShopType()
        shopTypeOfficial.name = "Official Store"
        shopTypeOfficial.isCheck = false
        
        shopTypes.append(shopTypeGold)
        shopTypes.append(shopTypeOfficial)
    }
    
    func setShopTypeCurrentData() {
        if let official = data[Query.official] as? Bool {
            let type = shopTypes.first(where: {$0.name == "Official Store"})
            type?.isCheck = official
        }
        
        if let fshop = data[Query.fshop] as? String {
            if fshop == "2" {
                let type = shopTypes.first(where: {$0.name == "Gold Merchant"})
                type?.isCheck = true
            }
        }
    }
}

extension ShopTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shop_type_cell", for: indexPath) as! ShopTypeCell
        
        let type = shopTypes[indexPath.row]
        cell.initiateCell(shopType: type)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let shop = shopTypes[indexPath.row]
        
        if shop.isCheck! {
            shop.isCheck = false
        } else {
            shop.isCheck = true
        }
        
        
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("did deselect")
        shopTypes[indexPath.row].isCheck = false
        
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    
}
